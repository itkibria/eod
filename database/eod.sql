﻿# Host: localhost  (Version 5.7.24)
# Date: 2019-12-03 18:45:26
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "data_types"
#

DROP TABLE IF EXISTS `data_types`;
CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "data_types"
#

INSERT INTO `data_types` VALUES (1,'users','users','User','Users','voyager-person','TCG\\Voyager\\Models\\User','TCG\\Voyager\\Policies\\UserPolicy','TCG\\Voyager\\Http\\Controllers\\VoyagerUserController','',1,0,NULL,'2019-11-25 11:57:08','2019-11-25 11:57:08'),(2,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','',1,0,NULL,'2019-11-25 11:57:08','2019-11-25 11:57:08'),(3,'roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,'','',1,0,NULL,'2019-11-25 11:57:08','2019-11-25 11:57:08'),(4,'eod_issues','eod','Eod Issue','Eod Issues',NULL,'App\\EODissueModel',NULL,'EODIssueController',NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2019-11-28 09:51:47','2019-12-01 07:44:49');

#
# Structure for table "data_rows"
#

DROP TABLE IF EXISTS `data_rows`;
CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "data_rows"
#

INSERT INTO `data_rows` VALUES (1,1,'id','number','ID',1,0,0,0,0,0,NULL,1),(2,1,'name','text','Name',1,1,1,1,1,1,NULL,2),(3,1,'email','text','Email',1,1,1,1,1,1,NULL,3),(4,1,'password','password','Password',1,0,0,1,1,0,NULL,4),(5,1,'remember_token','text','Remember Token',0,0,0,0,0,0,NULL,5),(6,1,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,6),(7,1,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,7),(8,1,'avatar','image','Avatar',0,1,1,1,1,1,NULL,8),(9,1,'user_belongsto_role_relationship','relationship','Role',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}',10),(10,1,'user_belongstomany_role_relationship','relationship','Roles',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}',11),(11,1,'settings','hidden','Settings',0,0,0,0,0,0,NULL,12),(12,2,'id','number','ID',1,0,0,0,0,0,NULL,1),(13,2,'name','text','Name',1,1,1,1,1,1,NULL,2),(14,2,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,3),(15,2,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,4),(16,3,'id','number','ID',1,0,0,0,0,0,NULL,1),(17,3,'name','text','Name',1,1,1,1,1,1,NULL,2),(18,3,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,3),(19,3,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,4),(20,3,'display_name','text','Display Name',1,1,1,1,1,1,NULL,5),(21,1,'role_id','text','Role',1,1,1,1,1,1,NULL,9);

#
# Structure for table "eod_issues"
#

DROP TABLE IF EXISTS `eod_issues`;
CREATE TABLE `eod_issues` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `issue_date` datetime DEFAULT NULL,
  `issue_details` longtext COLLATE utf8mb4_unicode_ci,
  `solution` longtext COLLATE utf8mb4_unicode_ci,
  `solution_provide_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `error_log_message` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "eod_issues"
#

INSERT INTO `eod_issues` VALUES (1,'2018-07-15 00:00:00','Unautorized Cheque Book Issue Record Found!!!!\nUnauthorized Entry Found in Instrument Register FP: 8003','1.Make auth_status_id \"D\"\nselect t.*, t.rowid from ultimus.cor_inst_reg_auth_log t where t.branch_id=0161 and t.auth_status_id=\'U\';\n2. Make auth_status_id \"A\"\nselect t.*, t.rowid from ultimus.cor_inst_reg_reqs t where  t.auth_status_id=\'U\';\n3. Remove Data','Ahad','NRBCB',NULL,NULL,NULL,NULL,NULL,NULL),(2,'2018-07-15 00:00:00','Ora-20000:Error processing ac:0125/243000021361\nERROR:OnDemand Charge Deduction Failed. Rule Id:0402 AC 0125-243000021361 AMT :1019.56','UPDATE rtl_time_ac_master t\n   SET t.principal_amt = t.current_balance_ccy\n WHERE t.branch_id = 0125\n   AND t.account_no = \'243000021361\';','Ahad','FSIBL',NULL,NULL,NULL,NULL,NULL,NULL),(3,'2018-07-02 00:00:00','PO Issued but transaction is not authorized & can’t appear in 1640 fast path in 0106 branch','update Register_Update_Flag =0; then the item shows in 8001 page & user can authorize.','Ahad','NRBCB',NULL,NULL,NULL,NULL,NULL,NULL),(4,'2018-07-02 00:00:00','Penalty Suspense Balance Mismatch with Suspense Register! Account No0009-70100000258 Account NO: 70100000258','Update :    The fields t.int_sus_amt_ccy,t.int_sus_amt_lcy,t.int_sus_amt_penal_ccy,t.int_sus_amt_penal_lcy are filled by 0. \nselect t.*, t.rowid from cor_lon_int_sus_master t where t.branch_id=10 and t.account_no=\'70100000258\';','Ahad','LankaBangla',NULL,NULL,NULL,NULL,NULL,NULL),(5,'2018-06-27 00:00:00','in FP 7153. messege showed Reversed IBT origining entry Missing','data insert in contra reg table','Jamil','Uttara',NULL,NULL,NULL,NULL,NULL,NULL),(6,'2018-06-27 00:00:00','Opening closing balance mismatch','update opening balance in master table as history balance','Jamil','Uttara',NULL,NULL,NULL,NULL,NULL,NULL),(7,'2018-06-25 00:00:00','Ora-20000: Error processing ac:0127/24300002232 ERROR:OnDemand Charge Deduction Failed. Rule ID:0233AC: 0127-24300002232 AMT:136.82','UPDATE rtl_time_ac_master t\n   SET t.principal_amt = t.current_balance_ccy\n WHERE t.branch_id = 0127\n   AND t.account_no = \'24300002232\';','Ahad','Premier',NULL,NULL,NULL,NULL,NULL,NULL),(8,'2018-05-20 00:00:00','Penalty Suspense Balance Mismatch with Suspense Register! Account No0010-70100000237 Account NO: 70100000237','Update :    The fields t.int_sus_amt_ccy,t.int_sus_amt_lcy,t.int_sus_amt_penal_ccy,t.int_sus_amt_penal_lcy are filled by 0. \nselect t.*, t.rowid from cor_lon_int_sus_master t where t.branch_id=10 and t.account_no=\'70100000237\';','Sanny','LankaBangla',NULL,NULL,NULL,NULL,NULL,NULL),(9,'2018-05-20 00:00:00','Mitford Branch Error: Failed Exception: ORA20000: at ULTIMUSISL.CHARGE_ADMIN, line7004','update ultimusisl.rtl_scheme_ac_mast t\n   set t.acc_status = \'M\'\n where t.branch_id = \'4002\'\n   and t.account_number in (\'74300038493\', \'74300038579\');\ncommit;','Sanny','SJIBL',NULL,NULL,NULL,NULL,NULL,NULL),(10,'2018-05-14 00:00:00','Ora-20000:Error processing ac:0010/22800000813 ERROR:OnDemand Charge Deduction Failed. Rule Id:0001 AC 0010-22800000813 AMT :21391.97','Update rtl_time_ac_master t set t.principal_amount=current_balance_ccy where t.branch_id=0010 and t.account_no=\'22800000813\'','Sanny','LankaBangla',NULL,NULL,NULL,NULL,NULL,NULL),(11,'2018-05-13 00:00:00','Day Close Problem : Batch file read and batch file read any branch','CBS admin was asked to check un-authorized data in authorize queue.','Jamil','Uttara',NULL,NULL,NULL,NULL,NULL,NULL),(12,'2018-05-13 00:00:00','Penalty Suspense Balance Mismatch with Suspense Register! Account No0010-70700000333 Account NO: 70700000333','Update :    The fields t.int_sus_amt_ccy,t.int_sus_amt_lcy,t.int_sus_amt_penal_ccy,t.int_sus_amt_penal_lcy are filled by 0. \nselect t.*, t.rowid from cor_lon_int_sus_master t where t.branch_id=10 and t.account_no=\'70700000333\';','Jamil','LankaBangla',NULL,NULL,NULL,NULL,NULL,NULL),(13,'2018-05-08 00:00:00','Day Close Problem : Reversed IBT Responding Entry Missing in [COR_IBT_CONTRA_REG] for Batch/Tracer: 392/2465.','update cor_trans_gl t set t.orgn_rspd_flag = null where t.branch_id=\'0001\' and t.batch_no=\'392\' and t.tracer_no = \'2465\';                          \nAfter day close :\nupdate cor_trans_gl t set t.orgn_rspd_flag = \'R\' where t.branch_id=\'0001\' and t.batch_no=',NULL,'LankaBangla',NULL,NULL,'Data collected for analysis and permanent fixation.',NULL,NULL,NULL),(14,'2018-05-07 00:00:00','Day Close Problem : Reversed IBT Responding Entry Missing in [COR_IBT_CONTRA_REG] for Batch/Tracer: 415/721.','Temporary Solution given. update cor_trans_gl t set t.orgn_rspd_flag = null where t.branch_id=\'0001\' and t.batch_no=\'415\' and t.tracer_no = \'721\';                            After Day Close: update cor_trans_gl t set t.orgn_rspd_flag = \'R\' where t.branch_',NULL,'LankaBangla',NULL,NULL,'Data collected for analysis and permanent fixation.',NULL,NULL,NULL),(15,'2018-05-07 00:00:00','EOD: Opening/Closing balance mismatch found in 1 Finance account(s)','Temporary Solution given.\nbegin\n  for h in (select t.* from cor_lon_ac_master_hist t \n    where t.branch_id =\'0010\' and t.account_no = \'72700000613\' and t.trans_date = \'06-May-2018\') loop\nupdate cor_lon_ac_master t\n   set t.closing_os_bal_ccy     = h.clos',NULL,'LankaBangla',NULL,NULL,'Data collected for analysis and permanent fixation.',NULL,NULL,NULL),(16,'2018-05-07 00:00:00','EOD: Penalty Suspense Balance Mismatch with suspense Register! Account no 0010-70700000054 Account no: 70700000054.','int_suspense_penal is ZERO in cor_lon_ac_master table,,,                        update cor_lon_int_sus_master t set t.int_sus_bal_ccy = 0, t.int_sus_bal_lcy = 0, t.int_sus_amt_ccy = 0, t.int_sus_amt_lcy = 0\nwhere t.branch_id=\'0010\' and t.account_no = \'707',NULL,'LankaBangla',NULL,NULL,'Data collected for analysis and permanent fixation.',NULL,NULL,NULL),(17,'2018-05-07 00:00:00','EOD: Mismatch Found in Transaction. Total Dr. : 1094979336.2, Total Cr. : 1094965346.64, Diff. : 13989.56','select t.status, t.*, t.rowid from TRS_GOV_SEC_PORTFOLIO t where t.maturity_dt=\'08-may-2018\';   Update status 1 to 5','Jamil','Premier',NULL,NULL,NULL,NULL,NULL,NULL),(18,'2019-11-28 00:00:00','EOD error for  Eskaton Branch (4033) (EOD date: 28-11-2019).','--Before EOD\r\n--Branch=\'4001\'\r\nupdate ultimusisl.rtl_scheme_ac_mast t\r\nset t.lien_amount = 0, t.blocked_amount=0\r\nwhere t.branch_id =\'4001\'\r\nand t.account_number =\'73500004152\' ;\r\ncommit;\r\n\r\n--branch=4004\r\nupdate ultimusisl.rtl_demand_ac_mast t set t.hold','Samith Binda Pantho','SJIBL',NULL,NULL,NULL,NULL,NULL,NULL),(19,'2019-11-27 00:00:00','EOD error for  Dhaka Main Branch(4001) (EOD date: 27-11-2019)','update ultimusisl.rtl_scheme_ac_mast t\nset t.acc_status = \'M\'\nwhere t.branch_id = 4001\nand t.account_number = \'76000001775\';\n\n','Md. Mahmudur Rashid','Shahjalal Islami Bank',NULL,NULL,NULL,NULL,NULL,NULL);

#
# Structure for table "failed_jobs"
#

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "failed_jobs"
#


#
# Structure for table "menus"
#

DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "menus"
#

INSERT INTO `menus` VALUES (1,'admin','2019-11-25 11:57:08','2019-11-25 11:57:08');

#
# Structure for table "menu_items"
#

DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "menu_items"
#

INSERT INTO `menu_items` VALUES (1,1,'Dashboard','','_self','voyager-boat',NULL,NULL,1,'2019-11-25 11:57:08','2019-11-25 11:57:08','voyager.dashboard',NULL),(2,1,'Media','','_self','voyager-images',NULL,NULL,5,'2019-11-25 11:57:08','2019-11-25 11:57:08','voyager.media.index',NULL),(3,1,'Users','','_self','voyager-person',NULL,NULL,4,'2019-11-25 11:57:08','2019-11-28 06:35:24','voyager.users.index',NULL),(4,1,'Roles','','_self','voyager-lock',NULL,NULL,3,'2019-11-25 11:57:08','2019-11-28 06:35:24','voyager.roles.index',NULL),(5,1,'Tools','','_self','voyager-tools',NULL,NULL,6,'2019-11-25 11:57:08','2019-11-28 06:35:24',NULL,NULL),(6,1,'Menu Builder','','_self','voyager-list',NULL,5,1,'2019-11-25 11:57:08','2019-11-28 06:35:24','voyager.menus.index',NULL),(7,1,'Database','','_self','voyager-data',NULL,5,2,'2019-11-25 11:57:08','2019-11-28 06:35:24','voyager.database.index',NULL),(8,1,'Compass','','_self','voyager-compass',NULL,5,3,'2019-11-25 11:57:08','2019-11-28 06:35:24','voyager.compass.index',NULL),(9,1,'BREAD','','_self','voyager-bread',NULL,5,4,'2019-11-25 11:57:08','2019-11-28 06:35:24','voyager.bread.index',NULL),(10,1,'Settings','','_self','voyager-settings',NULL,NULL,7,'2019-11-25 11:57:08','2019-11-28 06:35:24','voyager.settings.index',NULL),(11,1,'Hooks','','_self','voyager-hook',NULL,5,5,'2019-11-25 11:57:10','2019-11-28 06:35:24','voyager.hooks',NULL),(13,1,'EOD Issues','/admin/eod','_self','voyager-lifebuoy','#000000',NULL,2,'2019-11-28 06:35:05','2019-11-28 06:47:43',NULL,'');

#
# Structure for table "migrations"
#

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "migrations"
#

INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_01_01_000000_add_voyager_user_fields',1),(4,'2016_01_01_000000_create_data_types_table',1),(5,'2016_05_19_173453_create_menu_table',1),(6,'2016_10_21_190000_create_roles_table',1),(7,'2016_10_21_190000_create_settings_table',1),(8,'2016_11_30_135954_create_permission_table',1),(9,'2016_11_30_141208_create_permission_role_table',1),(10,'2016_12_26_201236_data_types__add__server_side',1),(11,'2017_01_13_000000_add_route_to_menu_items_table',1),(12,'2017_01_14_005015_create_translations_table',1),(13,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',1),(14,'2017_03_06_000000_add_controller_to_data_types_table',1),(15,'2017_04_21_000000_add_order_to_data_rows_table',1),(16,'2017_07_05_210000_add_policyname_to_data_types_table',1),(17,'2017_08_05_000000_add_group_to_settings_table',1),(18,'2017_11_26_013050_add_user_role_relationship',1),(19,'2017_11_26_015000_create_user_roles_table',1),(20,'2018_03_11_000000_add_user_settings',1),(21,'2018_03_14_000000_add_details_to_data_types_table',1),(22,'2018_03_16_000000_make_settings_value_nullable',1),(23,'2019_08_19_000000_create_failed_jobs_table',1);

#
# Structure for table "password_resets"
#

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "password_resets"
#


#
# Structure for table "permissions"
#

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "permissions"
#

INSERT INTO `permissions` VALUES (1,'browse_admin',NULL,'2019-11-25 11:57:09','2019-11-25 11:57:09'),(2,'browse_bread',NULL,'2019-11-25 11:57:09','2019-11-25 11:57:09'),(3,'browse_database',NULL,'2019-11-25 11:57:09','2019-11-25 11:57:09'),(4,'browse_media',NULL,'2019-11-25 11:57:09','2019-11-25 11:57:09'),(5,'browse_compass',NULL,'2019-11-25 11:57:09','2019-11-25 11:57:09'),(6,'browse_menus','menus','2019-11-25 11:57:09','2019-11-25 11:57:09'),(7,'read_menus','menus','2019-11-25 11:57:09','2019-11-25 11:57:09'),(8,'edit_menus','menus','2019-11-25 11:57:09','2019-11-25 11:57:09'),(9,'add_menus','menus','2019-11-25 11:57:09','2019-11-25 11:57:09'),(10,'delete_menus','menus','2019-11-25 11:57:09','2019-11-25 11:57:09'),(11,'browse_roles','roles','2019-11-25 11:57:09','2019-11-25 11:57:09'),(12,'read_roles','roles','2019-11-25 11:57:09','2019-11-25 11:57:09'),(13,'edit_roles','roles','2019-11-25 11:57:09','2019-11-25 11:57:09'),(14,'add_roles','roles','2019-11-25 11:57:09','2019-11-25 11:57:09'),(15,'delete_roles','roles','2019-11-25 11:57:09','2019-11-25 11:57:09'),(16,'browse_users','users','2019-11-25 11:57:09','2019-11-25 11:57:09'),(17,'read_users','users','2019-11-25 11:57:09','2019-11-25 11:57:09'),(18,'edit_users','users','2019-11-25 11:57:09','2019-11-25 11:57:09'),(19,'add_users','users','2019-11-25 11:57:09','2019-11-25 11:57:09'),(20,'delete_users','users','2019-11-25 11:57:09','2019-11-25 11:57:09'),(21,'browse_settings','settings','2019-11-25 11:57:09','2019-11-25 11:57:09'),(22,'read_settings','settings','2019-11-25 11:57:09','2019-11-25 11:57:09'),(23,'edit_settings','settings','2019-11-25 11:57:09','2019-11-25 11:57:09'),(24,'add_settings','settings','2019-11-25 11:57:09','2019-11-25 11:57:09'),(25,'delete_settings','settings','2019-11-25 11:57:09','2019-11-25 11:57:09'),(26,'browse_hooks',NULL,'2019-11-25 11:57:10','2019-11-25 11:57:10'),(27,'browse_eod_issues','eod_issues','2019-11-28 09:51:47','2019-11-28 09:51:47'),(28,'read_eod_issues','eod_issues','2019-11-28 09:51:47','2019-11-28 09:51:47'),(29,'edit_eod_issues','eod_issues','2019-11-28 09:51:47','2019-11-28 09:51:47'),(30,'add_eod_issues','eod_issues','2019-11-28 09:51:47','2019-11-28 09:51:47'),(31,'delete_eod_issues','eod_issues','2019-11-28 09:51:47','2019-11-28 09:51:47');

#
# Structure for table "roles"
#

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "roles"
#

INSERT INTO `roles` VALUES (1,'admin','Administrator','2019-11-25 11:57:08','2019-11-25 11:57:08'),(2,'user','Normal User','2019-11-25 11:57:08','2019-11-25 11:57:08');

#
# Structure for table "permission_role"
#

DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "permission_role"
#

INSERT INTO `permission_role` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1);

#
# Structure for table "settings"
#

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "settings"
#

INSERT INTO `settings` VALUES (1,'site.title','bread\\index.blade.php','Leads Late Night Support Issues','','text',1,'Site'),(2,'site.description','Site Description','Late Night Support','','text',2,'Site'),(3,'site.logo','Site Logo','','','image',3,'Site'),(4,'site.google_analytics_tracking_id','Google Analytics Tracking ID',NULL,'','text',4,'Site'),(5,'admin.bg_image','Admin Background Image','','','image',5,'Admin'),(6,'admin.title','Admin Title','Late Night Support','','text',1,'Admin'),(7,'admin.description','Admin Description','Welcome to Voyager. The Missing Admin for Laravel','','text',2,'Admin'),(8,'admin.loader','Admin Loader','','','image',3,'Admin'),(9,'admin.icon_image','Admin Icon Image','','','image',4,'Admin'),(10,'admin.google_analytics_client_id','Google Analytics Client ID (used for admin dashboard)',NULL,'','text',1,'Admin');

#
# Structure for table "translations"
#

DROP TABLE IF EXISTS `translations`;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "translations"
#


#
# Structure for table "users"
#

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "users"
#

INSERT INTO `users` VALUES (1,1,'kibria','akm.kibria@gmail.com','users/default.png',NULL,'$2y$10$g.Hy540K6jSsuUvtva4JzeaXz1BS3o0mvpPVVvhP6BO.L6KoT6Mza',NULL,NULL,'2019-11-25 12:11:27','2019-11-25 12:11:27'),(2,2,'Masud','mehajabin.masud@leads-bd.com','users/default.png',NULL,'$2y$10$Eb3u.M0Ow380fqkqnrxNLOtsFqSMruiwyXwkaIMIhzHcJCq3J1wO6',NULL,'{\"locale\":\"en\"}','2019-11-27 11:04:20','2019-11-27 11:45:48');

#
# Structure for table "user_roles"
#

DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `user_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "user_roles"
#

