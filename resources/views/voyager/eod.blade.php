@extends('voyager::master')

{{--@section('page_title', __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))--}}

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    {{-- <h1 class="page-title">
         <i class="{{ $dataType->icon }}"></i>
         {{ __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
     </h1>--}}
@stop

@section('content')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-lock"></i> Roles
        </h1>
        <a href="http://eod.test/admin/roles/create" class="btn btn-success btn-add-new">
            <i class="voyager-plus"></i> <span>Add New</span>
        </a>
        <a class="btn btn-danger" id="bulk_delete_btn"><i class="voyager-trash"></i> <span>Bulk Delete</span></a>


        <!-- /.modal -->

        <script>
            window.onload = function () {
                // Bulk delete selectors
                var $bulkDeleteBtn = $('#bulk_delete_btn');
                var $bulkDeleteModal = $('#bulk_delete_modal');
                var $bulkDeleteCount = $('#bulk_delete_count');
                var $bulkDeleteDisplayName = $('#bulk_delete_display_name');
                var $bulkDeleteInput = $('#bulk_delete_input');
                // Reposition modal to prevent z-index issues
                $bulkDeleteModal.appendTo('body');
                // Bulk delete listener
                $bulkDeleteBtn.click(function () {
                    var ids = [];
                    var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('.select_all');
                    var count = $checkedBoxes.length;
                    if (count) {
                        // Reset input value
                        $bulkDeleteInput.val('');
                        // Deletion info
                        var displayName = count > 1 ? 'Roles' : 'Role';
                        displayName = displayName.toLowerCase();
                        $bulkDeleteCount.html(count);
                        $bulkDeleteDisplayName.html(displayName);
                        // Gather IDs
                        $.each($checkedBoxes, function () {
                            var value = $(this).val();
                            ids.push(value);
                        })
                        // Set input value
                        $bulkDeleteInput.val(ids);
                        // Show modal
                        $bulkDeleteModal.modal('show');
                    } else {
                        // No row selected
                        toastr.warning('You haven&#039;t selected anything to delete');
                    }
                });
            }
        </script>
    </div>
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <div id="dataTable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="dataTables_length" id="dataTable_length"><label>Show <select
                                                    name="dataTable_length" aria-controls="dataTable"
                                                    class="form-control input-sm">
                                                    <option value="10">10</option>
                                                    <option value="25">25</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                </select> entries</label></div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div id="dataTable_filter" class="dataTables_filter"><label>Search:<input
                                                    type="search" class="form-control input-sm" placeholder=""
                                                    aria-controls="dataTable"></label></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="dataTable" class="table table-hover dataTable no-footer" role="grid"
                                               aria-describedby="dataTable_info">
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1"
                                                    colspan="1" aria-label="

                                            : activate to sort column ascending" style="width: 15px;">
                                                    <input type="checkbox" class="select_all">
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1"
                                                    colspan="1" aria-label="
                                                                                        Essue date
                                                                                    : activate to sort column ascending"
                                                    style="width: 44px;">
                                                    Essue date
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1"
                                                    colspan="1" aria-label="
                                                                                        Email
                                                                                    : activate to sort column ascending"
                                                    style="width: 208px;">
                                                    Issue Details
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1"
                                                    colspan="1" aria-label="
                                                                                        Created At
                                                                                    : activate to sort column ascending"
                                                    style="width: 125px;">
                                                    Customer Name
                                                </th>

                                                <th class="actions text-right sorting_disabled" rowspan="1" colspan="1"
                                                    aria-label="Actions" style="width: 199px;">Actions
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>


                                            <tr role="row" class="odd">
                                                <td>
                                                    <input type="checkbox" name="row_id" id="checkbox_2" value="2">
                                                </td>
                                                <td>
                                                    <div>Masud</div>
                                                </td>
                                                <td>
                                                    <div>mehajabin.masud@leads-bd.com</div>
                                                </td>
                                                <td>
                                                </td>
                                                <td class="no-sort no-click" id="bread-actions">
                                                    <a href="javascript:;" title="Delete"
                                                       class="btn btn-sm btn-danger pull-right delete" data-id="2"
                                                       id="delete-2">
                                                        <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">Delete</span>
                                                    </a>
                                                    <a href="http://eod.test/admin/users/2/edit" title="Edit"
                                                       class="btn btn-sm btn-primary pull-right edit">
                                                        <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Edit</span>
                                                    </a>
                                                    <a href="http://eod.test/admin/users/2" title="View"
                                                       class="btn btn-sm btn-warning pull-right view">
                                                        <i class="voyager-eye"></i> <span class="hidden-xs hidden-sm">View</span>
                                                    </a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="dataTables_info" id="dataTable_info" role="status"
                                             aria-live="polite">Showing 1 to 2 of 2 entries
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="dataTables_paginate paging_simple_numbers" id="dataTable_paginate">
                                            <ul class="pagination">
                                                <li class="paginate_button previous disabled" aria-controls="dataTable"
                                                    tabindex="0" id="dataTable_previous"><a href="#">Previous</a></li>
                                                <li class="paginate_button active" aria-controls="dataTable"
                                                    tabindex="0"><a href="#">1</a></li>
                                                <li class="paginate_button next disabled" aria-controls="dataTable"
                                                    tabindex="0" id="dataTable_next"><a href="#">Next</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        });
    </script>
@stop

