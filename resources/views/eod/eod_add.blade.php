@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <form id="contact-form" action="{{ url('/eod') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="messages"></div>
                    <div class="controls">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_name">Issue Date</label>
                                    <input id="form_name" type="text" name="issue_date" class="form-control"
                                           placeholder="Please enter EOD date" required="required"
                                           data-error="Firstname is required.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_lastname">Solution Provide By</label>
                                    <input id="form_lastname" type="text" name="solution_provide_by"
                                           class="form-control"
                                           required="required">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_email">Customer Name</label>
                                    <input id="form_email" type="text" name="customer_name" class="form-control"
                                           placeholder="Please enter your Customer Name" required="required">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="remarks">Remarks</label>
                                    <input id="remarks" type="tel" name="remarks" class="form-control">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="form_message">Issue Details</label>
                                    <input id="form_message" name="Issue Details" class="form-control" rows="4" required
                                           data-error="Please, enter Issue Details"></input>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="form_message">Error Log Message</label>
                                    <input id="form_message" name="Error_Log_Message" class="form-control" rows="4"
                                           required
                                           data-error="Please, enter Issue Details"></input>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="form_message">Solution</label>
                                    <textarea id="form_message" name="solution" class="form-control" rows="4" required
                                              data-error="Please, give solution."></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-success btn-send" value="Save Issues">
                            </div>
                        </div>

                    </div>

                </form>

            </div>

        </div>
    </div>
@endsection
