@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="page-content container-fluid">
            <button type="button" class="btn btn-success btn-block">Add New</button>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Issues date</th>
                    <th>Title</th>
                    <th>Solution</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($eod as $value)
                    <tr>
                        <td>{{$value->issue_date}}</td>
                        <td>{{$value->issue_details}}</td>
                        <td>{{$value->solution}}</td>
                        <td>
                            <button type="button" class="btn btn-primary btn-sm">View</button>
                            <button type="button" class="btn btn-warning btn-sm">Edit</button>
                            <button type="button" class="btn btn-danger btn-sm">Delete</button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection






















