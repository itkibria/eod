<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EODissueModel extends Model
{
    protected $table = 'eod_issues';
}
