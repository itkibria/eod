<?php

namespace App\Http\Controllers\Voyager;

use App\EODissueModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EODIssueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $eod = EODissueModel::paginate(30);
        return view('eod.eod', compact('eod'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('eod.eod_add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'issue_date' => 'required',
            'issue_details' => 'required',
            'solution' => 'required',
            'solution_provide_by' => 'required',
            'customer_name' => 'required',
            'error_log_message' => 'required',
            'remarks' => 'required',
        ]);

        try {
            dd($request);
            $eod = $request->except('_token');
            dd($request);
            /*$eod['phone'] = "+880" . substr($request->phone, -10);*/
            /*$eod['photograph'] = Storage::url('') . $request->photograph->store('rental/drivers');*/
            /*$eod['driving_license_front'] = Storage::url('') . $request->dl_front->store('rental/drivers');*/
            /*$eod['driving_license_back'] = Storage::url('') . $request->dl_back->store('rental/drivers');*/
            /*$eod['active'] = 1;*/
            EODissueModel::create($request);

            /*return redirect('eod')->with('flash_success', 'Issues added successfully !');*/
        } catch (Exception $e) {
            dd($request);
            /*return redirect('eod/create')->with('flash_error', $e->getMessage());*/
        }

    }

    /**
     * Display the specified resource.
     *
     * @param \App\EODissueModel $eODissueModel
     * @return \Illuminate\Http\Response
     */
    public function show(EODissueModel $eODissueModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\EODissueModel $eODissueModel
     * @return \Illuminate\Http\Response
     */
    public function edit(EODissueModel $eODissueModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\EODissueModel $eODissueModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EODissueModel $eODissueModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\EODissueModel $eODissueModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(EODissueModel $eODissueModel)
    {
        //
    }
}
