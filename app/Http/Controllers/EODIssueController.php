<?php

namespace App\Http\Controllers;

use App\EODissueModel;
use Illuminate\Http\Request;

class EODIssueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $eod = EODissueModel::paginate(30);
        return view('eod.eod', compact('eod'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('eod.eod_add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'issue_date' => 'required',
            'issue_details' => 'required',
            'solution' => 'required',
            'solution_provide_by' => 'required',
            'customer_name' => 'required',
            'error_log_message' => 'required',
            'remarks' => 'required',
        ]);

        dd($request);
        try {
            /*$eod = $request->except('_token');*/
            /*$eod['phone'] = "+880" . substr($request->phone, -10);*/
            /*$eod['photograph'] = Storage::url('') . $request->photograph->store('rental/drivers');*/
            /*$eod['driving_license_front'] = Storage::url('') . $request->dl_front->store('rental/drivers');*/
            /*$eod['driving_license_back'] = Storage::url('') . $request->dl_back->store('rental/drivers');*/
            /*$eod['active'] = 1;*/
            /*EODissueModel::create($request);*/


            $eod = new EODissueModel;

            $eod->issue_date = $request->input('issue_date');
            $eod->issue_details = $request->input('issue_details');
            $eod->solution = $request->input('solution');
            $eod->solution_provide_by = $request->input('solution_provide_by');
            $eod->customer_name = $request->input('customer_name');
            $eod->error_log_message = $request->input('error_log_message');
            $eod->remarks = $request->input('remarks');

            dd($eod);

            $eod->save();
            return redirect('eod')->with('flash_success', 'Issues added successfully !');
        } catch (Exception $e) {
            return redirect('eod/create')->with('flash_error', $e->getMessage());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
